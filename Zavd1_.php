<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First page</title>

    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th{
            background-color: yellow;
        }
    </style>
</head>
<body>
<?php
echo "<table>
        <tr>
            <th>X'y</th>
            <th>X!</th>
            <th>my_tg(x)</th>
            <th>sin(x)</th>
            <th>cos(x)</th>
            <th>tg(x)</th>
            <th>x+y</th>
            <th>x-y</th>
            <th>x*y</th>
            <th>x/y</th>
            <th>Average(x,y)</th>
       </tr>";

if($_POST){
    (float)$vX = $_POST["valueX"];
    (float)$vY = $_POST["valueY"];

    $cislo = $vX;
    $fact = $cislo;
    $ffact = 1;
    $mt = microtime();
    while($fact >= 1)
    {
        $ffact = $fact * $ffact;
        $fact--;
    }
    $md=number_format(microtime()-$mt, 6);

    $tan = number_format(sin($vX)/cos($vX),11);

    $array = array($vX,$vY);
    $average = array_sum($array) / count($array);
    $zP = $vX + $vY;
    $zM = $vX - $vY;

    echo "<tr>";
        echo"<td>".pow($vX, $vY)."</td>";
        echo"<td>".$ffact."</td>";
        echo"<td>".$tan."</td>";
        echo"<td>".sin($vX)."</td>";
        echo"<td>".cos($vX)."</td>";
        echo"<td>".tan($vX)."</td>";
        echo"<td>".$zP."</td>";
        echo"<td>".$zM."</td>";
        echo"<td>".$vX * $vY."</td>";
        echo"<td>".$vX/$vY."</td>";
        echo"<td>".$average."</td>";
    echo "</tr>";
    echo "</table>";
}
?>
    <br>
<form action="" method="post">
    <input type="text" id="fx" name="valueX">
    <input type="text" id="fy" name="valueY">
    <input type="submit">
</form>
</html>