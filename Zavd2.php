<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Second page</title>
    <style>
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif, sans-serif;
            font-size: 14px;
            border-radius: 20px;
            border-spacing: 0;
            text-align: center;

        }
        td:nth-child(odd) {
            background-color: <?php printf( "#%06X\n", mt_rand( 0, 0xFFFFFF )); ?>;
        }
        td:nth-child(even) {
            background-color: <?php printf( "#%06X\n", mt_rand( 0, 0xFFFFFF )); ?>;
        }
        tr:nth-child(odd) {
            background-color: <?php printf( "#%06X\n", mt_rand( 0, 0xFFFFFF )); ?>;
        }
        tr:nth-child(even) {
            background-color: <?php printf( "#%06X\n", mt_rand( 0, 0xFFFFFF )); ?>;
        }
        td {
            border-style: solid;
            border-width: 1px ;
            border-color: black;

        }
    </style>
</head>
<body>
<?php

function table($rows, $cols)
{
    $n = 1;
    echo "<table >\n";
    for ($i = 1; $i <= $rows; $i++) {

        echo "\t<tr \">\n";
        for ($j = 1; $j <= $cols; $j++) {

            echo "\t\t<td  >{$n}</td>\n";
            $n ++;
        }
        echo "\t</tr>\n";
    }
    echo "</table>\n";

}
if($_POST){
    $rows = $_POST["rows"];
    $cols = $_POST["cols"];
    table($rows,$cols );
}
?>
<form method="post">
    <p> <input name="rows" type="text">   <input name="cols" type="text">  <input class="but" value="Відтворити" type="submit">  </p>
</form>
</body>
</html>
